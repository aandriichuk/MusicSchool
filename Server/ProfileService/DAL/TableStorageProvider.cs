﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.WindowsAzure.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProfileService.DAL
{
    public class TableStorageProvider : ITableStorageProvider
    {
        private readonly IHostingEnvironment _env;

        public TableStorageProvider(IHostingEnvironment env)
        {
            _env = env;
        }

        private IConfigurationRoot _config;

        public IConfigurationRoot GetConfig()
        {
            if (_config != null)
                return _config;

            var builder = new ConfigurationBuilder()
                           .SetBasePath(_env.ContentRootPath)
                           .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                           .AddJsonFile($"appsettings.{_env.EnvironmentName}.json", optional: true)
                           .AddEnvironmentVariables();

            _config = builder.Build();

            return _config;
        }

        public void SetupTables()
        {
            var config = GetConfig();

            var storageConnectionString = config["ConnectionStrings:StorageConnectionString"];

            // Parse the connection string and return a reference to the storage account.
            var storageAccount = CloudStorageAccount.Parse(storageConnectionString);

            // Create the table client.
            var tableClient = storageAccount.CreateCloudTableClient();

            // Retrieve a reference to the table.
            var table = tableClient.GetTableReference("Profiles");

            // Create the table if it doesn't exist.
            table.CreateIfNotExists();
        }
    }
}
