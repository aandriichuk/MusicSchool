﻿using Microsoft.WindowsAzure.Storage.Table;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProfileService.DAL.Entities
{
    public class Profile : TableEntity
    {
        public Profile(string lastName, string firstName)
        {
            this.PartitionKey = lastName;
            this.RowKey = firstName;
        }

        public Profile() { }

        public string Email { get; set; }

        public string PhoneNumber { get; set; }
    }
}
