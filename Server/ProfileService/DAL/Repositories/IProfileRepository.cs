﻿using ProfileService.DAL.Entities;
using System.Collections.Generic;

namespace ProfileService.DAL.Repositories
{
    public interface IProfileRepository
    {
        void SaveProfile(Profile profile);

        Profile GetProfile(string firstName, string lastName);

        void DeleteProfile(string firstName, string lastName);

        List<Profile> GetAllProfiles();
    }
}
