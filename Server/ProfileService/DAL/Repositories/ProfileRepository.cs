﻿using ProfileService.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProfileService.DAL.Repositories
{
    public class ProfileRepository : IProfileRepository
    {
        public void SaveProfile(Profile profile)
        {
            throw new NotImplementedException();
        }

        public Profile GetProfile(string firstName, string lastName)
        {
            throw new NotImplementedException();
        }

        public void DeleteProfile(string firstName, string lastName)
        {
            throw new NotImplementedException();
        }

        public List<Profile> GetAllProfiles()
        {
            throw new NotImplementedException();
        }
    }
}
